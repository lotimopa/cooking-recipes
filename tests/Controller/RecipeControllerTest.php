<?php

namespace App\Tests\Controller;

use App\Repository\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 *
 */
class RecipeControllerTest extends WebTestCase
{
    public function testNotFound(): void
    {
        $client = static::createClient();

        $client->request('GET', '/recette/'.PHP_INT_MAX);

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
