# Cooking Recipes

## Installation

```
git clone git@gitlab.com:delny/cooking-recipes.git
cd cooking-recipes
```

```
cp .env .env.local
cp .env.test .env.test.local
```

Renseigner les informations de base de données

```
composer install -o
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
symfony server:start
```

## Fixtures

```
php bin/console doctrine:fixtures:load
```

## assets

```
yarn install
yarn build
```
